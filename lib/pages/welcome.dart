import 'package:datamate/pages/login.dart';
import 'package:datamate/pages/registration.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(image: DecorationImage(image: AssetImage("images/background.jpg"), fit: BoxFit.cover)),
        width: double.maxFinite,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Welcome to DataMate", textAlign: TextAlign.center,  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700, color: Colors.white),),

              const SizedBox(height: 20,),
              const Text("Login to your account or register a new account", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w300, color: Colors.white),),
              const SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                children: [

                  Expanded(
                    child: MaterialButton(
                      height: 50,
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: ((context) => const LoginPage())));
                    },
                    child: const Text("Login", style: TextStyle(color: Colors.white)),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50), side: const BorderSide(color: Colors.white)),
                    ),
                  ),
              
                  const SizedBox(width: 20,),
              
                  Expanded(
                    child: MaterialButton(
                      height: 50,
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: ((context) => const RegistrationPage())));
                    },
                    child: const Text("Registration", style: TextStyle(color: Colors.white)),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50), side: const BorderSide(color: Colors.white)),
                    ),
                  ),
              
              
                  ]
              ),
        
        
            ],
          ),
        ),
      ),
    );
  }
}