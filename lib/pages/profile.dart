import 'package:flutter/material.dart';

class MyProfilePage extends StatelessWidget {
  const MyProfilePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("My Profile"),),
      body: Container(
        color: Colors.transparent,
        width: double.maxFinite,
        height: MediaQuery.of(context).size.height,

        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 100,
                height: 100,
                color: Colors.transparent,

                child: const CircleAvatar(backgroundImage: AssetImage("images/profile.jpeg")),
                
              ),
              
              const SizedBox(height: 20,),

              Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text("Susan McLareen", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),
                      SizedBox(width: 10,),
                      Icon(Icons.edit)
                    ],
                  ),

              const SizedBox(height: 20,),

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Expanded(
                    child: MaterialButton(
                      onPressed: () {},
                      shape: RoundedRectangleBorder(side: const BorderSide(color: Colors.blueAccent), borderRadius: BorderRadius.circular(15) ),
                      child: const Text('Delete Image'),
                      ),
                  ),

                    const SizedBox(width: 20,),

                    Expanded(
                      child: MaterialButton(
                      onPressed: () {},
                      shape: RoundedRectangleBorder(side: const BorderSide(color: Colors.blueAccent), borderRadius: BorderRadius.circular(15) ),
                      child: const Text('Change Image'),
                      ),
                    ),
                ],
              ),

              const SizedBox(height: 20,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text("Email Address: Susanmclareen@yahoo.com"),
                  Icon(Icons.edit),
                ],
              ),

              const SizedBox(height: 5,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text("Password: *******"),
                  Icon(Icons.edit),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}