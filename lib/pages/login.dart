import 'package:datamate/pages/home.dart';
import 'package:datamate/pages/registration.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(iconTheme: const IconThemeData(color: Colors.black), title: const Text("Account Login", style: TextStyle(color: Color.fromARGB(255, 44, 41, 41))), centerTitle: true, backgroundColor: Colors.transparent, elevation: 0,),
      body: SingleChildScrollView(
        physics: const ClampingScrollPhysics(),
        child: Container(
          color: Colors.transparent,
          width: double.maxFinite,
          height: MediaQuery.of(context).size.height,
      
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
      
              children: [
                const Center(child: Text("Enter the required details to login to your account", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300), textAlign: TextAlign.center,)),
                const SizedBox(height: 20,),
                const Text("Email Address", style: TextStyle(fontWeight: FontWeight.w300),),
                const SizedBox(height: 10,),
                const TextField(decoration: InputDecoration(border: OutlineInputBorder(), hintText: "Enter your email address"),),
      
                const SizedBox(height: 20,),
                const Text("Password", style: TextStyle(fontWeight: FontWeight.w300),),
                const SizedBox(height: 10,),
                const TextField(decoration: InputDecoration(border: OutlineInputBorder(), hintText: "Enter your password"),),

                const SizedBox(height: 5,),
                Center(child: TextButton(onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: ((context) => const RegistrationPage())));
                }, 
                child: const Text("Don't have an account? Register"))
                ),
                const SizedBox(height: 5,),

                ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: ((context) => const HomePage())));
                  }, 
                  child: const Text("Login"),
                  style: ElevatedButton.styleFrom(minimumSize: const Size(double.maxFinite, 50)),

                  ),

                const SizedBox(height: 5,),
                Center(child: TextButton(onPressed: () {}, child: const Text("Forgot password? Reset"))),
                const SizedBox(height: 5,),
      
      
      
              ],
            ),
          ),
        ),
      ),
      
    );
  }
}