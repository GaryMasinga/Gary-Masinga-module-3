import 'package:datamate/pages/login.dart';
import 'package:flutter/material.dart';
import 'home.dart';

class RegistrationPage extends StatelessWidget {
  const RegistrationPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(iconTheme: const IconThemeData(color: Colors.black), title: const Text("Account Registration", style: TextStyle(color: Colors.black),), centerTitle: true, backgroundColor: Colors.transparent, elevation: 0),
      body: SingleChildScrollView(
        physics: const ClampingScrollPhysics(),
        child: Container(
          color: Colors.transparent,
          width: double.maxFinite,
          height: MediaQuery.of(context).size.height,
      
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
      
              children: [
                const Center(child: Text("Enter the required details to register a new account", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300), textAlign: TextAlign.center,)),
                const SizedBox(height: 20,),
                const Text("Email Address", style: TextStyle(fontWeight: FontWeight.w300),),
                const SizedBox(height: 10,),
                const TextField(decoration: InputDecoration(border: OutlineInputBorder(), hintText: "Enter your email address"),),
      
                const SizedBox(height: 20,),
                const Text("Password", style: TextStyle(fontWeight: FontWeight.w300),),
                const SizedBox(height: 10,),
                const TextField(decoration: InputDecoration(border: OutlineInputBorder(), hintText: "Create a password"),),
      
                const SizedBox(height: 20,),
                const Text("Password confirmation", style: TextStyle(fontWeight: FontWeight.w300),),
                const SizedBox(height: 10,),
                const TextField(decoration: InputDecoration(border: OutlineInputBorder(), hintText: "Confirm your password"),),

                const SizedBox(height: 5,),
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const LoginPage()));
                    },
                    child: const Text("Already have an account? Login")
                    )
                    ),
                const SizedBox(height: 5,),

                ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomePage()));
                  }, 
                  child: const Text("Create Account"),
                  style: ElevatedButton.styleFrom(minimumSize: const Size(double.maxFinite, 50)),

                  ),


      
      
      
              ],
            ),
          ),
        ),
      ),
    );
  }
}