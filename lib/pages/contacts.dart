import 'package:flutter/material.dart';

class ContactUs extends StatelessWidget {
  const ContactUs({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Contact Us"),),
      body: InkWell(
        onTap: () {},
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Card(
            elevation: 20,
            child: Container(
              color: Colors.transparent,
              width: double.maxFinite,
              height: double.maxFinite,

              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: const [

                  Text("Email Address", style: TextStyle(fontSize: 20),),
                  Text("Datamate@it.com", style: TextStyle(),),

                  SizedBox(height: 20,),

                  Text("Telephone", style: TextStyle(fontSize: 20),),
                  Text("0127553500", style: TextStyle(),),
                  
                  SizedBox(height: 20,),

                  Text("Twitter", style: TextStyle(fontSize: 20),),
                  Text("@Datamate122", style: TextStyle(),),

                  SizedBox(height: 20,),

                  Text("Web", style: TextStyle(fontSize: 20),),
                  Text("Datamate.co.za", style: TextStyle(),),

                  SizedBox(height: 20,),

                  Text("WhatsApp", style: TextStyle(fontSize: 20),),
                  Text("0821236598", style: TextStyle(),),
                ],
              ),
            ),
          ),
        ),

      ),
      
    );
  }
}