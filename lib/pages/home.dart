import 'package:datamate/pages/courses.dart';
import 'package:datamate/pages/profile.dart';
import 'package:flutter/material.dart';

import 'contacts.dart';

class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(image: DecorationImage(image: AssetImage("images/profile.jpeg"), fit: BoxFit.cover)),
              height: MediaQuery.of(context).size.height * 0.3,
              width: double.maxFinite,
      
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
      
                  children: const [
                     Text("Hi, Susan", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300, color: Colors.white),),
                    
                  ],
                ),
              ),
      
            ),

            Container(
              color: Colors.transparent,
              width: double.maxFinite,
              height: MediaQuery.of(context).size.height * 0.3,
              
              child: Row(
                children: [

                  Expanded(
                    child: InkWell(
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => const CoursesPage())),
                      child: Card(
                        elevation: 20,
                        child: const Center(child: Text("My Courses", style: TextStyle(fontSize: 20),)),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      )
                      ),
                  ),

                    Expanded(
                      child: InkWell(
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => const MyProfilePage())),
                      child: Card(
                        elevation: 20,
                        child: const Center(child: Text("My Profile", style: TextStyle(fontSize: 20))),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      )
                      ),
                    ),
                ]
              ),

              ),


              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 20, bottom: 20),
                  child: Container(
                    width: double.maxFinite,
                    color: Colors.transparent,
              
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        FloatingActionButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => const ContactUs()));
                          },
                          child: const Icon(Icons.phone),
              
                          )
                      ],
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}